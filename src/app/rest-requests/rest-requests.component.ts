import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {ItemData} from '../ItemData';

@Component({
  selector: 'app-rest-requests',
  templateUrl: './rest-requests.component.html',
  styleUrls: ['./rest-requests.component.css']
})
export class RestRequestsComponent implements OnInit {

  rid = '';
  uid = '';
  ticker = '';

  latestRequest = '';

  singularResponse: ItemData;
  pluralResponse: ItemData [] = [];

  extraRobotId = '';
  extraButtons = false;

  constructor(private rs: RestService) {
  }

  ngOnInit(): void {
  }

  getRobotById() {
    const that = this;
    this.rs.getRobotById(this.rid)
      .subscribe(response => {
        console.log(response);
        that.latestRequest = 'GET robot/{id}';
        that.singularResponse = new ItemData(JSON.stringify(response));
      });
  }

  getRobotByUserAndTicker() {
    const that = this;
    this.rs.getRobotsByUserAndTicker(this.uid, this.ticker)
      .subscribe(response => {
        console.log(response);
        that.latestRequest = 'GET robots?user=&ticker=';
        that.pluralResponse = response;
      });
  }

  getRobotsByUser() {
    const that = this;
    this.rs.getRobotsByUser(this.uid)
      .subscribe(response => {
        console.log(response);
        that.latestRequest = 'GET user/{id}/robots';
        that.pluralResponse = response;
      });
  }

  lastGetRobots() {
    return this.latestRequest === 'GET robots?user=&ticker=' || this.latestRequest === 'GET user/{id}/robots';
  }

  lastGetRobot() {
    return this.latestRequest === 'GET robot/{id}';
  }

  favor() {
    this.rs.favorRobot(this.extraRobotId)
      .subscribe(response => {
        this.singularResponse = response;
        this.pluralResponse.push(response);
      });
  }

  activate() {
    this.rs.activateRobot(this.extraRobotId)
      .subscribe(response => {
        this.singularResponse = response;
        const upd = this.pluralResponse.find(x => x.robot_id === response.robot_id);
        const index = this.pluralResponse.indexOf(upd);
        this.pluralResponse[index] = response;
      });
  }

  deactivate() {
    this.rs.deactivateRobot(this.extraRobotId)
      .subscribe(response => {
        this.singularResponse = response;
        const upd = this.pluralResponse.find(x => x.robot_id === response.robot_id);
        const index = this.pluralResponse.indexOf(upd);
        this.pluralResponse[index] = response;
      });
  }

  toggleExtraButtons(clickedRobot: string) {
    this.extraButtons = !this.extraButtons;
    if (this.extraButtons) {
      if (clickedRobot === undefined) {
        this.extraRobotId = this.singularResponse.robot_id + '';
      } else {
        this.extraRobotId = clickedRobot;
      }
    } else {
      this.extraRobotId = '';
    }
  }
}
