export class ItemData {
  robot_id: number;
  owner_user_id: number;
  parent_robot_id: number;
  is_favourite: boolean;
  is_active: boolean;
  ticker: string;
  buy_price: number;
  sell_price: number;
  plan_start: string;
  plan_end: string;
  plan_yield: number;
  fact_yield: number;
  deals_count: number;

  constructor(rawData: any) {
    const st = rawData.toString();
    const parsedMessage = JSON.parse(st) as ItemData;

    this.robot_id = parsedMessage.robot_id;
    this.owner_user_id = parsedMessage.owner_user_id;
    this.parent_robot_id = parsedMessage.parent_robot_id;
    this.is_favourite = parsedMessage.is_favourite;
    this.is_active = parsedMessage.is_active;
    this.ticker = parsedMessage.ticker;
    this.buy_price = parsedMessage.buy_price;
    this.sell_price = parsedMessage.sell_price;
    this.plan_start = parsedMessage.plan_start;
    this.plan_end = parsedMessage.plan_end;
    this.plan_yield = parsedMessage.plan_yield;
    this.plan_yield = parsedMessage.plan_yield;
    this.fact_yield = parsedMessage.fact_yield;
    this.deals_count = parsedMessage.deals_count;
  }
}
