import {Component, OnInit} from '@angular/core';
import {ItemData} from '../ItemData';
import {WebsocketService} from '../websocket.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-item-info-singular',
  templateUrl: './item-info-singular.component.html',
  styleUrls: ['./item-info-singular.component.css']
})
export class ItemInfoSingularComponent implements OnInit {

  connected = false;
  isClose = new Subscription();
  subscription = new Subscription();
  messages: ItemData[] = [];

  rid = '';

  constructor(public socketService: WebsocketService) {
  }


  ngOnInit(): void {
    this.subscription = this.socketService.getSubscriptionGlobal()
      .subscribe((itemData: ItemData) => this.messages.push(itemData));
    this.isClose = this.socketService.getOnCloseSubject()
      .subscribe(value => {
        if (value) {
          this.connected = false;
        }
      });
  }

  onConnectToOne() {
    this.socketService.initSocket('ws://localhost:8000/ws/robot/' + this.rid);
    this.connected = true;
  }

  onReOpen(): void {
    this.socketService.gentlyClose();
    this.socketService.initSocket('ws://localhost:8000/ws/robot/' + this.rid);
    this.messages = [];
  }

  closeGently() {
    this.socketService.gentlyClose();
  }

  disconnect() {
    this.connected = false;
  }
}
