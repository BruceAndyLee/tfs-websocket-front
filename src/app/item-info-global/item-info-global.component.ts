import {WebsocketService} from '../websocket.service';
import {Component, OnInit} from '@angular/core';
import {ItemData} from '../ItemData';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-item-info',
  templateUrl: './item-info-global.component.html',
  styleUrls: ['./item-info-global.component.css']
})
export class ItemInfoGlobalComponent implements OnInit {

  connected = false;
  isClose = new Subscription();
  subscription = new Subscription();
  messages: ItemData[] = [];

  user = '';
  ticker = '';

  constructor(public socketService: WebsocketService) {
  }


  ngOnInit(): void {
    this.subscription = this.socketService.getSubscriptionGlobal()
      .subscribe((itemData: ItemData) => this.messages.push(itemData));
    this.isClose = this.socketService.getOnCloseSubject()
      .subscribe(value => {
        if (value) {
          this.connected = false;
        }
      });
  }

  onConnectToAll() {
    this.initIoConnection();
    this.connected = true;
  }

  private initIoConnection(): void {
    this.socketService.initSocket('ws://localhost:8000/ws/robots');

  }

  onReOpen(): void {
    this.socketService.gentlyClose();
    this.socketService.initSocket('ws://localhost:8000/ws/robots?user=' + this.user + '&ticker=' + this.ticker);
    this.messages = [];

  }

  disconnectFromAll() {
    this.connected = false;
  }

  closeGently(){
    this.socketService.gentlyClose();
  }
}
