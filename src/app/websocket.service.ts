import {ItemData} from './ItemData';
import {Observable, Subject} from 'rxjs';
import {Injectable} from '@angular/core';

const GENTLY_CLOSE = 4001;

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  public socket;
  public subscriptionGlobal = new Subject<ItemData>();
  public onCloseSubject = new Subject<boolean>();

  public initSocket(url: string): void {
    this.socket = new WebSocket(url);
    this.socket.addEventListener('open', (event) => {
      console.log('Websocket opened ', event);
    });

    const that = this;

    this.socket.addEventListener('message', (event) => {
      console.log('Message from server ', event.data);

      const it = new ItemData(event.data);
      console.log(it);
      that.subscriptionGlobal.next(it);
    });

    this.socket.addEventListener('close', (event) => {
      console.log('Websocket is closed ', event);
      if (event.code !== GENTLY_CLOSE){
        that.onCloseSubject.next(true);
      }

    });
  }

  public getSubscriptionGlobal(): Observable<ItemData> {
    return this.subscriptionGlobal.asObservable();
  }

  public getOnCloseSubject(): Observable<boolean> {
    return this.onCloseSubject.asObservable();
  }

  public gentlyClose(): void {
    this.socket.close(GENTLY_CLOSE);
    const that = this;
    that.onCloseSubject.next(false);
  }

}
