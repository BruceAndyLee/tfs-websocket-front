import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { MatListModule} from '@angular/material/list';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ItemInfoGlobalComponent } from './item-info-global/item-info-global.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule} from '@angular/material/button';
import { ItemInfoSingularComponent } from './item-info-singular/item-info-singular.component';
import { MatInputModule} from '@angular/material/input';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { SigninPageComponent } from './signin-page/signin-page.component';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { RestRequestsComponent } from './rest-requests/rest-requests.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';


const config: SocketIoConfig = { url: 'http://localhost:4444', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    ItemInfoGlobalComponent,
    ItemInfoSingularComponent,
    SigninPageComponent,
    RestRequestsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    SocketIoModule.forRoot(config),
    MatCardModule,
    HttpClientModule,
    MatTabsModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
