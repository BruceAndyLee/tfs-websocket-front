import {Component, OnInit} from '@angular/core';
import {RestService} from '../rest.service';
import {UserCreds} from '../userCreds';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signin-page',
  templateUrl: './signin-page.component.html',
  styleUrls: ['./signin-page.component.css']
})
export class SigninPageComponent implements OnInit {

  email: string;
  password: string;

  constructor(
    private rs: RestService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  signin() {
    this.rs.signin(new UserCreds(this.email, this.password))
      .subscribe(response => {
        console.log(response.bearer);
        localStorage.setItem('bearer', response.bearer);
        console.log(localStorage.getItem('bearer'));
      });

    this.router.navigate(['rest']);
  }
}
