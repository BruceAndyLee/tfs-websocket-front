import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemInfoGlobalComponent } from './item-info-global/item-info-global.component';
import {ItemInfoSingularComponent} from './item-info-singular/item-info-singular.component';
import {SigninPageComponent} from './signin-page/signin-page.component';
import {RestRequestsComponent} from './rest-requests/rest-requests.component';


const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  { path: 'signin', component: SigninPageComponent },
  { path: 'rest', component: RestRequestsComponent },
  { path: 'items', component: ItemInfoGlobalComponent },
  { path: 'item', component: ItemInfoSingularComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
