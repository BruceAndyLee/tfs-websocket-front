export class Bearer {
  bearer: string;

  constructor(bearer: string) {
    this.bearer = bearer;
  }
}
