import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserCreds} from './userCreds';
import {ItemData} from './ItemData';
import {tap} from 'rxjs/operators';
import {Bearer} from './bearer';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  backendUrl = 'http://localhost:8000/api/v1';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  };

  constructor(private http: HttpClient) {
  }

  signin(credentials: UserCreds) {
    return this.http.post<Bearer>(this.backendUrl + '/signin', credentials, this.httpOptions)
      .pipe(
        tap(
          (response: Bearer) => localStorage.setItem('bearer', response.bearer)
        )
      );
  }

  // Get user/id/robots
  // Get robots?user=&ticker=
  // Get robot/id

  getRobotsByUser(uid: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    return this.http.get<[ItemData]>(this.backendUrl + '/user/' + uid + '/robots', {headers: httpHeaders});
  }

  getRobotsByUserAndTicker(uid: string, ticker: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    return this.http.get<ItemData[]>(this.backendUrl + '/robots?user=' + uid + '&ticker=' + ticker, {headers: httpHeaders});
  }

  getRobotById(rid: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    return this.http.get<ItemData>(this.backendUrl + '/robot/' + rid, {headers: httpHeaders});
  }

  // PUT robot/{id}/activate
  activateRobot(rid: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    const data = JSON.parse('{}');
    return this.http.put<ItemData>(this.backendUrl + '/robot/' + rid + '/activate', data, {headers: httpHeaders});
  }

  // PUT robot/{id}/deactivate
  deactivateRobot(rid: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    const data = JSON.parse('{}');
    return this.http.put<ItemData>(this.backendUrl + '/robot/' + rid + '/deactivate', data, {headers: httpHeaders});
  }

  // PUT robot/{id}/favourite
  favorRobot(rid: string) {
    const httpHeaders = this.httpOptions.headers.append('Bearer', localStorage.getItem('bearer'));
    const data = JSON.parse('{}');
    return this.http.put<ItemData>(this.backendUrl + '/robot/' + rid + '/favourite', data, {headers: httpHeaders});

  }
}
